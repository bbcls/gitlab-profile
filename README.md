# 🌷 WetSeeds
### Groupe 3 - B3 DEV2 - MSPR EPSI 2023 [TPRE500 Développement déploiement appli IA]

### Installation et utilisation

Pour mettre en place l'entireté de la stack technique, utilisez [https://gitlab.com/bbcls/wetseeds-stack](https://gitlab.com/bbcls/wetseeds-stack) et suivez ses instructions.

Pour une installation manuelle, chaque projet est pourvu dans sa description, des commandes à exécuter pour le lancer.

### Composition du Groupe 3

- TEMPLE Clément
- DUTHEIL Luc
- TOMASIAN Nathan
- CETIN Begüm
- CASTAYBERT Stanislas